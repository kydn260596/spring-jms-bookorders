package com.kevin.config;

import com.kevin.listener.BookOrderProcessingMessageListener;
import com.kevin.pojo.Book;
import com.kevin.pojo.BookOrder;
import com.kevin.pojo.Customer;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.jms.connection.SingleConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.jms.ConnectionFactory;

@EnableTransactionManagement // Habilitar la gestion de transacciones en JMS
@EnableJms
@Configuration
public class JmsConfig /**implements JmsListenerConfigurer*/ {

    // JmsListenerConfigurer - Sirve para registrar un custom listener

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsConfig.class);

    @Value("${spring.activemq.broker-url}")
    private String brokenUrl;

    @Value("${spring.activemq.user}")
    private String user;

    @Value("${spring.activemq.password}")
    private String password;


    // Desanotar este bean ya que produciria conflictos de parseo, por ejemplo:
    // Cannot convert object of type [com.kevin.pojo.BookOrder] to JMS message. Supported message payloads are: String, byte array, Map<String,?>, Serializable object
    @Bean
    public MessageConverter jacksonJmsMessageConverter(){
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    // XML Message Converter
    /*@Bean
    public MessageConverter xmlMarshallingMessageConverter(){
        MarshallingMessageConverter converter = new MarshallingMessageConverter(xmlMarshaller());
        converter.setTargetType(MessageType.TEXT);
        return converter;
    }*/

    /*@Bean
    public XStreamMarshaller xmlMarshaller(){
        XStreamMarshaller marshaller = new XStreamMarshaller();
        marshaller.setSupportedClasses(Book.class, Customer.class, BookOrder.class);
        return marshaller;
    }*/

    // Define JmsTemplate Bean
    /**
    @Bean
    public JmsTemplate jmsTemplate(){
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        return template;
    }
     */

    // Define ConectionFactory Bean - ActiveMQ
    // Primera forma de configurar una conexion a un MoM, usar la propia conexion del servidor (ActiveMQ)
    /*@Bean
    public ActiveMQConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setUserName(user);
        connectionFactory.setPassword(password);
        connectionFactory.setBrokerURL(brokenUrl);
        return connectionFactory;
    }*/

    // Segunda forma de configurar una conexion a un MoM
    /*@Bean
    public SingleConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, brokenUrl);
        SingleConnectionFactory singleConnectionFactory = new SingleConnectionFactory(connectionFactory);
        singleConnectionFactory.setReconnectOnException(true);
        singleConnectionFactory.setClientId("myClientId");
        return singleConnectionFactory;
    }*/

    // Tercera forma de configurar una conexion a un MoM
    @Bean
    public CachingConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, brokenUrl);
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(connectionFactory);
        cachingConnectionFactory.setClientId("StoreFront");
        cachingConnectionFactory.setSessionCacheSize(100);
        return cachingConnectionFactory;
    }


    // Define ListenerConnectionFactory Bean
    @Bean
    public DefaultJmsListenerContainerFactory listenerContainerFactory(){
        DefaultJmsListenerContainerFactory listenerContainerFactory = new DefaultJmsListenerContainerFactory();
        listenerContainerFactory.setConnectionFactory(connectionFactory());
        listenerContainerFactory.setMessageConverter(jacksonJmsMessageConverter());
        listenerContainerFactory.setTransactionManager(jmsTransactionManager());
        listenerContainerFactory.setErrorHandler(t -> {
            LOGGER.info("Handling error in listener for messsages, error: " + t.getMessage());
        });
        // For xML Conversion
        // listenerContainerFactory.setMessageConverter(xmlMarshallingMessageConverter());
        return listenerContainerFactory;
    }

    /*@Bean
    public BookOrderProcessingMessageListener jmsMessageListener(){
        BookOrderProcessingMessageListener listener = new BookOrderProcessingMessageListener();
        return listener;
    }*/

    // Crear bean para la gestion de transacciones en JMS
    @Bean
    public PlatformTransactionManager jmsTransactionManager(){
        return new JmsTransactionManager(connectionFactory());
    }

    @Bean
    public JmsTemplate jmsTemplate(){
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory());
        jmsTemplate.setMessageConverter(jacksonJmsMessageConverter());
        jmsTemplate.setDeliveryPersistent(true);
        jmsTemplate.setSessionTransacted(true);
        return jmsTemplate;
    }

    /**
    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
        SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
        endpoint.setMessageListener(jmsMessageListener());
        endpoint.setDestination("book.order.processed.queue");
        endpoint.setId("book-order-processed-queue");
        endpoint.setSubscription("my-subscription");
        endpoint.setConcurrency("1");
        registrar.setContainerFactory(listenerContainerFactory());
        registrar.registerEndpoint(endpoint, listenerContainerFactory());
    }
     */
}
