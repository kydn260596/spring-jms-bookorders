package com.kevin;

import com.kevin.service.jms.Sender;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@EnableJms // Habilitar la tecnologia JMS, y el envio de mensajes JMS
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    // SpringBootServletInitializer class
    // Esta es una extensión de WebApplicationInitializer
    // que ejecuta SpringApplication desde un archivo WAR tradicional
    // implementado en un contenedor web. Esta clase enlaza beans Servlet,
    // Filter y ServletContextInitializer del contexto de la aplicación al servidor.


    public static void main(String[] args){
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        /**
        Sender sender = context.getBean(Sender.class);
        System.out.println("Preparing to send a message");
        sender.sendMessage("order-queue", "item: 1234, customer: 1234");
        */
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

}
