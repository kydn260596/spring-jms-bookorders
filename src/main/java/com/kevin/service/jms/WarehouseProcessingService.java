package com.kevin.service.jms;

import com.kevin.pojo.BookOrder;
import com.kevin.pojo.ProcessedBookOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.adapter.JmsResponse;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class WarehouseProcessingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WarehouseProcessingService.class);

    private static final String PROCESSED_QUEUE = "book.order.processed.queue";
    private static final String CANCELED_QUEUE = "book.order.canceled.queue";

    @Autowired
    private JmsTemplate jmsTemplate;

    @Transactional // Que sea gestionado por JmsTransactionManager
    public JmsResponse<Message<ProcessedBookOrder>> processOrder(BookOrder bookOrder, String storeId, String orderState){

        Message<ProcessedBookOrder> message;
        if(orderState.equals("NEW")){
            message =  add(bookOrder, storeId);
            return JmsResponse.forQueue(message, PROCESSED_QUEUE);
        } else if (orderState.equals("UPDATE")){
            message =  update(bookOrder, storeId);
            return JmsResponse.forQueue(message, PROCESSED_QUEUE);
        } else if (orderState.equals("DELETE")){
            message =  delete(bookOrder, storeId);
            return JmsResponse.forQueue(message, CANCELED_QUEUE);
        } else {
            throw new IllegalArgumentException(
                    "WarehouseProcessingService.processOrder, el estado de la orden no coincide con los valores esperados");
        }
    }

    public Message<ProcessedBookOrder> add(BookOrder bookOrder, String storeId){
        LOGGER.info("Adding a new order to the database");
        return build(new ProcessedBookOrder(
                bookOrder, new Date(), new Date()), "ADDED", storeId);
    }

    public Message<ProcessedBookOrder> update(BookOrder bookOrder, String storeId){
        LOGGER.info("Updating an order from the database");
        return build(new ProcessedBookOrder(
                bookOrder, new Date(), new Date()), "UPDATED", storeId);
    }

    public Message<ProcessedBookOrder> delete(BookOrder bookOrder, String storeId){
        LOGGER.info("Deleting an order from the database");
        return build(new ProcessedBookOrder(
                bookOrder, new Date(), null), "DELETED", storeId);
    }

    public Message<ProcessedBookOrder> build(ProcessedBookOrder processedBookOrder,
                                             String orderState, String storeId){
        return MessageBuilder
                .withPayload(processedBookOrder)
                .setHeader("orderState", orderState)
                .setHeader("storeId", storeId)
                .build();
    }
}
