package com.kevin.service.jms;

import com.kevin.pojo.BookOrder;
import com.kevin.pojo.ProcessedBookOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.listener.adapter.JmsResponse;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

@Service
public class WarehouseReceiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(WarehouseReceiver.class);

    @Autowired
    WarehouseProcessingService warehouseProcessingService;

    // Synchronous Reception
    @JmsListener(destination = "book.order.queue")
    // @SendTo("book.order.processed.queue")
    public JmsResponse<Message<ProcessedBookOrder>> receive(@Payload BookOrder bookOrder,
                                                           @Header String bookOrderId,
                                                           @Header String storeId,
                                                           @Header String orderState
                                               /*, MessageHeaders messageHeaders*/) throws IllegalAccessException {
        LOGGER.info("Received a message");
        LOGGER.info("Message is == " + bookOrder);
        LOGGER.info("Message property orderState == {}, bookOrderId == {}, storeId == {}", orderState, bookOrderId, storeId);
        // LOGGER.info("messageHeaders = {} ", messageHeaders);

        if(bookOrder.getBook().getTitle().startsWith("L")){
            throw new IllegalAccessException("OrderId=" + bookOrder.getBookOrderId() + " begins with L and these books are not allowed!");
        }

        return warehouseProcessingService.processOrder(bookOrder, storeId, orderState);
    }

    // Al ser este receiver tambien un desencadenante para enviar el mensaje a la siguiente cola
    // la respuesta seria de tipo JmsResponse, ya que la siguiente cola sera determinada en
    // tiempo de ejecucion y esa clase es la apropiada para esa labor
}
